class BreedsController < ApplicationController
  before_action only: [:edit, :update, :destroy]

  def index
    @breed = DogBreedFetcher.fetch(params[:name])
    @breeds = DogBreedFetcher.list
  end
end
